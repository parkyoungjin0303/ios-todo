//
//  ViewController.swift
//  iOSTodo
//
//  Created by 박영진 on 10/09/2020.
//  Copyright © 2020 박영진. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CreateTaskDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(headerView)
        view.addSubview(createButton)
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        createButton.addTarget(self, action: #selector(self.createButtonTouchUpInside(_:)), for: .touchUpInside)
        
        let safeGuide = view.safeAreaLayoutGuide
        let contentGuide = scrollView.contentLayoutGuide
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: safeGuide.topAnchor),
            headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            headerView.rightAnchor.constraint(equalTo: view.rightAnchor),
            headerView.widthAnchor.constraint(equalToConstant: view.frame.width),
            headerView.heightAnchor.constraint(equalToConstant: 44),
            
            createButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            createButton.leftAnchor.constraint(equalTo: view.leftAnchor),
            createButton.rightAnchor.constraint(equalTo: view.rightAnchor),
            createButton.widthAnchor.constraint(equalToConstant: view.frame.width),
            createButton.heightAnchor.constraint(equalToConstant: 44),
            
            scrollView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            scrollView.bottomAnchor.constraint(equalTo: createButton.topAnchor),
            scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),
            scrollView.widthAnchor.constraint(equalToConstant: view.frame.width),
            
            contentGuide.topAnchor.constraint(equalTo: stackView.topAnchor),
            contentGuide.leftAnchor.constraint(equalTo: view.leftAnchor),
            contentGuide.rightAnchor.constraint(equalTo: view.rightAnchor),
            contentGuide.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
        ])
        
        loadTodo()
    }
    
    func createTask(item: String) {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.text = item
        v.numberOfLines = 0
        v.textColor = .black
        v.textAlignment = .center
        stackView.addArrangedSubview(v)
        
        NSLayoutConstraint.activate([
            v.widthAnchor.constraint(equalToConstant: view.frame.width)
        ])
    }
    
    @objc private func createButtonTouchUpInside(_ sender: UIButton!) {
        let vc = CreateTaskViewController()
        vc.createTaskDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    private func loadTodo() {
        UserDefaultUtil().all.forEach { createTask(item: $0) }
    }
    
    private let headerView: UILabel = {
        let v = UILabel()
        v.textAlignment = .center
        v.translatesAutoresizingMaskIntoConstraints = false
        v.text = "TODO"
        return v
    }()
    
    private let createButton: UIButton = {
        let v = UIButton()
        v.setTitleColor(.black, for: .normal)
        v.setTitle("Create task", for: .normal)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private let stackView: UIStackView = {
        let v = UIStackView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.axis = .vertical
        return v
    }()
}
