//
//  CreateTaskViewController.swift
//  iOSTodo
//
//  Created by 박영진 on 14/09/2020.
//  Copyright © 2020 박영진. All rights reserved.
//

import UIKit

protocol CreateTaskDelegate {
    
    func createTask(item: String)
}

class CreateTaskViewController: UIViewController {
    
    var createTaskDelegate: CreateTaskDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
    
        view.addSubview(contentView)
        view.addSubview(createButton)
        
        createButton.addTarget(self, action: #selector(self.createButtonTouchUpInside(_:)), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: createButton.topAnchor),
            contentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            contentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
            contentView.widthAnchor.constraint(equalToConstant: view.frame.width),
            
            createButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            createButton.leftAnchor.constraint(equalTo: view.leftAnchor),
            createButton.rightAnchor.constraint(equalTo: view.rightAnchor),
            createButton.widthAnchor.constraint(equalToConstant: view.frame.width),
            createButton.heightAnchor.constraint(equalToConstant: 44)
        ])
    }
    
    private let contentView: UITextView = {
        let v = UITextView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.font = .init(descriptor: .init(), size: 18)
        return v
    }()
    
    private let createButton: UIButton = {
        let v = UIButton()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.setTitle("Completion", for: .normal)
        v.setTitleColor(.black, for: .normal)
        return v
    }()
    
    @objc private func createButtonTouchUpInside(_ sender: UIButton!) {
        if (!contentView.text.isEmpty) {
            UserDefaultUtil().addTodo(contentView.text)
            createTaskDelegate?.createTask(item: contentView.text)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
