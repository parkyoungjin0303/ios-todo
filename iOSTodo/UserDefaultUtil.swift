//
//  UserDefaultUtil.swift
//  iOSTodo
//
//  Created by 박영진 on 14/09/2020.
//  Copyright © 2020 박영진. All rights reserved.
//

import Foundation

class UserDefaultUtil {
    
    let mUserDefaults = UserDefaults.standard
    
    var all: [String] {
        get {
            mUserDefaults.stringArray(forKey: KEY_TODO) ?? [String]()
        }
    }
    
    func addTodo(_ todo: String) {
        var all = mUserDefaults.stringArray(forKey: KEY_TODO) ?? [String]()
        all.append(todo)
        mUserDefaults.set(all, forKey: KEY_TODO)
    }
    
    private let KEY_TODO = "KEY_TODO"
}
